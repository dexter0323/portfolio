import { Fragment } from 'react';
import Image from 'next/image';

export const Skill = ({ item, iconSize }: any) => {
  const { img, name, bg } = item;
  return (
    <Fragment>
      <div className="flex flex-col items-center bg-black_main p-4 space-y-3 rounded-lg">
        <div className="flex items-center grow">
          <Image
            src={`/skills/${img}.svg`}
            width={0}
            height={0}
            alt={`skill ${name}`}
            priority={true}
            className={`${bg ? 'bg-' + bg : ''} w-7 sm:w-8 md:w-10 lg:w-12`}
          />
        </div>
        <div className="text-sm text-white text-center">{name}</div>
      </div>
    </Fragment>
  );
};
