/* eslint-disable react/no-unescaped-entities */
export const About = () => (
  <div
    id="about"
    className="flex flex-col justify-between pt-10 mx-10 lg:flex-row text-white"
  >
    <div className="lg:w-6/12">
      <h2 className="text-xl md:text-2xl text-center md:text-left">
        👋 Hi, thanks for your visit!
      </h2>
      <div className="mt-10 lg:mt-16 text-lg md:text-xl font-extralight text-justify">
        <p>
          I see myself as a committed, focused, optimistic, and results-driven
          individual, very passionate about delivering high-quality products
          that not only meet but also surpass customers' expectations.
        </p>
        <br />
        <p className="hidden lg:block">
          I enjoy considering development challenges are similar to the
          understanding of the matter, where breaking it down into atoms is
          essential for analysis. Similarly, in software development, breaking
          work into manageable tasks. Both atoms and Software Development are
          integral parts of more intricate systems that continuously evolve and
          adapt to changes.
        </p>
      </div>
      {
        //  Al igual que los átomos, el desarrollo de software comparte una
        //  estructura compleja y fundamental para su funcionamiento. Mientras un
        //  átomo cuenta con un núcleo, electrones y protones, el software se basa
        //  en componentes como algoritmos, datos y códigos que interactúan entre
        //  sí para realizar tareas específicas. La estabilidad en ambos casos es
        //  esencial: el átomo busca alcanzar un equilibrio en sus
        //  configuraciones, mientras que el software lo logra a través de pruebas
        //  y optimizaciones. Tanto los átomos como el desarrollo de software
        //  forman parte de un sistema más complejo que evoluciona y se adapta
        //  constantemente a los cambios. Esta analogía resalta la fascinante
        //  interconexión entre el microcosmos y el mundo digital.
      }
    </div>
    <div className="atom-wrapper mt-5 md:mt-10 flex items-center justify-center relative lg:w-6/12">
      <div className="atom w-64 h-64 md:w-96 md:h-96">
        <div className="electron"></div>
        <div className="electron-alpha"></div>
        <div className="electron-omega"></div>
      </div>
      <span className="credit absolute text-sm" rel="Jorge Moreno">
        @alterebro
      </span>
    </div>
  </div>
);
