'use client';

import { SwitchTheme } from '@/app/components/SwitchTheme';
import Image from 'next/image';
import Link from 'next/link';
import { useState, useEffect } from 'react';

export const Header = ({ data }: any) => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    window.addEventListener(
      'resize',
      () => window.innerWidth >= 640 && setOpen(false)
    );
  }, []);

  return (
    <header className="m-5">
      <ul className="flex flex-row justify-between items-center text-white">
        <li className="min-w-max">
          <Image
            src="/logo.svg"
            width={30}
            height={0}
            alt="logo"
            priority={true}
          />
        </li>
        <li>
          <ul
            className={`${
              open
                ? 'flex-col absolute top-20 left-1/2 transform -translate-x-1/2 text-center space-y-5 bg-black_second w-full py-16'
                : 'hidden'
            } text-xl sm:text-lg sm:static sm:flex sm:flex-row sm:justify-between sm:space-x-5 font-light tracking-wide`}
          >
            {data.map((item: any, i: number) => (
              <li key={i} className="hover:text-blue_aqua">
                <Link href={item.ref} onClick={() => setOpen(false)}>
                  {item.name}
                </Link>
              </li>
            ))}
          </ul>
        </li>
        <li className="hidden sm:block">{/* <SwitchTheme /> */}</li>
        <li className="sm:hidden text-slate-300" onClick={() => setOpen(!open)}>
          {open ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-8 h-8 text-blue_aqua"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-8 h-8"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
              />
            </svg>
          )}
        </li>
      </ul>
    </header>
  );
};
