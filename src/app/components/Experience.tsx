'use client';

import {
  VerticalTimeline,
  VerticalTimelineElement,
} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import Image from 'next/image';

export const Experience = ({ data }: any) => {
  return (
    <div id="experience" className="!tracking-wide py-10 md:mt-10">
      <h3 className="text-white text-xl font-thin ml-5 mb-5 sm:text-left lg:text-center md:ml-10 sm:mb-5 md:text-2xl lg:text-3xl xl:my-10 xl:text-4xl">
        Experience & Education
      </h3>
      <VerticalTimeline>
        {data.map((element: any, i: number) => {
          let isWorkIcon = element.icon === 'work';
          // let showButton =
          //   element.buttonText !== undefined &&
          //   element.buttonText !== null &&
          //   element.buttonText !== '';

          return (
            <VerticalTimelineElement
              key={i}
              id={isWorkIcon ? '' : 'education'}
              className="text-slate-700"
              iconStyle={{ background: isWorkIcon ? '#99F8FF' : '#B3E6C7' }}
              icon={
                isWorkIcon ? (
                  <Image
                    src={`/work.svg`}
                    alt="wtever"
                    width={0}
                    height={0}
                    className="w-3/5 m-auto h-full"
                  />
                ) : (
                  <Image
                    src={`/school.svg`}
                    alt="wtever"
                    width={0}
                    height={0}
                    className="w-3/5 m-auto h-full"
                  />
                )
              }
            >
              <h3 className="!text-slate-800 !text-base lg:!text-xl vertical-timeline-element-title">
                {element.title}
              </h3>
              <h5 className="!text-slate-800 !text-sm lg:!text-lg vertical-timeline-element-subtitle">
                {element.location}
              </h5>
              <p
                className="text-slate-800 !text-base !font-thin lg:!text-lg"
                id="description"
              >
                {element.description}
              </p>
              <span className="!text-black_main !text-sm !pb-0 lg:!text-white lg:!text-lg vertical-timeline-element-date">
                {element.date}
              </span>
              {/* {showButton && (
                <a
                  className={`button ${isWorkIcon ? WorkIcon : SchoolIcon}`}
                  href="/"
                >
                  {element.buttonText}
                </a>
              )} */}
            </VerticalTimelineElement>
          );
        })}
      </VerticalTimeline>
    </div>
  );
};
