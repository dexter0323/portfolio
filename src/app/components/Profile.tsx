import Image from 'next/image';
import Link from 'next/link';

export const Profile = () => (
  <div className="flex flex-col items-center justify-center md:justify-between md:flex-row md:items-center px-10 py-5 md:py-10 lg:py-20 bg-black_second">
    <div className="flex flex-col space-y-5 items-center tracking-wide text-center md:text-left md:flex-row md:space-x-20">
      <div className="w-1/2 mx-auto rounded-full md:m-0 md:w-1/5 lg:w-1/6">
        <Image
          src="/juan.png"
          alt="Juan"
          width="0"
          height="0"
          className="w-full h-auto overflow-hidden"
          priority={true}
        />
      </div>
      <div className="flex flex-col md:space-y-6 lg:space-y-14">
        <div>
          <h2 className="text-xl md:text-2xl lg:text-4xl text-blue_aqua">
            Juan Sanchez
          </h2>
          <h2 className="text-lg mt-1 md:text-xl lg:text-2xl lg:mt-2 font-light text-white">
            Software Engineer
          </h2>
        </div>
        <div className="hidden md:flex flex-row text-gray_text space-x-10">
          <div className="flex space-x-3">
            <div className="text-4xl font-semibold text-slate-300">+8</div>
            <div className="text-sm lg:text-base font-extralight leading-5 uppercase text-slate-300">
              years of
              <br />
              experience
            </div>
          </div>
          <div className="flex space-x-3">
            <div className="text-4xl font-semibold text-slate-300">+25</div>
            <div className="text-sm lg:text-base font-extralight leading-5 uppercase text-slate-300">
              participation in projects from
              <br />
              u.s. and dom. rep. companies
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="flex mt-3 space-x-5 md:flex-col lg:mt-0 md:space-x-0 md:space-y-10 md:justify-center">
      <Link href="https://www.linkedin.com/in/juansanchez23/" target="_blank">
        <Image
          src="/linkedin.svg"
          width={24}
          height={0}
          alt="logo"
          className="md:w-10"
          priority={true}
        />
      </Link>
      <Link href="https://gitlab.com/dexter0323" target="_blank">
        <Image
          src="/skills/gitlab-bw.svg"
          width={24}
          height={0}
          alt="logo"
          className="md:w-10"
          priority={true}
        />
      </Link>
      <Link href="https://github.com/dexter0323" target="_blank">
        <Image
          src="/skills/github.svg"
          width={24}
          height={0}
          alt="logo"
          className="md:w-10"
          priority={true}
        />
      </Link>
    </div>
  </div>
);
