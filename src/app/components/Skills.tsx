'use client';

import { Fragment, useState, useEffect } from 'react';
import { Skill } from '@/app/components/Skill';
import Link from 'next/link';

export const Skills = ({ data }: any) => {
  const [selected, setSelected] = useState(0);
  const [bigScreen, setBigScreen] = useState(false);

  useEffect(() => {
    window.innerWidth >= 1024 && setBigScreen(true);
    window.addEventListener('resize', () =>
      window.innerWidth >= 1024 ? setBigScreen(true) : setBigScreen(false)
    );
  }, []);

  return (
    <>
      <div
        id="skills"
        className="flex flex-col py-10 px-5 space-y-5 bg-black_second tracking-wide mt-32 lg:p-10 lg:mt-36 lg:flex-row lg:justify-between lg:space-x-10 lg:space-y-0"
      >
        {data.map((s: any) => (
          <Link
            id={`skill${s.id}`}
            key={s.id}
            onClick={() =>
              bigScreen && setSelected(selected === s.id ? 0 : s.id)
            }
            href={`#skill${s.id}`}
            className="outline-none"
          >
            <div
              className={`${
                selected === s.id
                  ? 'bg-blue_aqua_light drop-shadow-lg lg:scale-105'
                  : 'bg-white'
              } group border-0 cursor-pointer p-5 rounded-lg text-black_main xl:p-10 lg:min-h-[340px] md:hover:bg-blue_aqua_light hover:drop-shadow-lg lg:hover:scale-105 ease-in duration-300`}
            >
              <div
                className="flex items-center space-x-5 lg:space-x-0 lg:space-y-5 lg:flex-col"
                onClick={() =>
                  !bigScreen && setSelected(selected === s.id ? 0 : s.id)
                }
              >
                <div
                  className={`${
                    selected === s.id
                      ? 'bg-white text-slate-600'
                      : 'text-slate-500 bg-blue_aqua_light'
                  } p-3   rounded-lg ease-in duration-300 lg:p-4 md:group-hover:text-slate-600 md:group-hover:bg-white`}
                >
                  {s.icon}
                </div>
                <div className="flex flex-col space-y-2 lg:space-y-5">
                  <h4 className="text-slate-700 group-hover:text-slate-900 ease-in duration-300 lg:text-xl lg:text-center">
                    {s.name}
                  </h4>
                  <p className="text-justify text-sm text-slate-500 group-hover:text-slate-700 font-extralight ease-in duration-300 lg:text-lg">
                    {s.description}
                  </p>
                </div>
              </div>
              {!bigScreen && (
                <div
                  className="mt-3 text-center font-semibold text-sm text-blue_aqua_dark"
                  onClick={() => setSelected(selected === s.id ? 0 : s.id)}
                >
                  {selected === s.id ? 'CLOSE' : 'SEE TECHS'}
                </div>
              )}
            </div>
            {!bigScreen && selected === s.id && (
              <div className="grid grid-cols-3 gap-5 py-5 md:grid-cols-4">
                {s.items.map((item: any, i: number) => (
                  <Skill key={i} item={item} />
                ))}
              </div>
            )}
          </Link>
        ))}
      </div>
      {bigScreen && selected ? (
        <div className="bg-black_second px-10 pb-10">
          <div className="grid grid-cols-5 gap-5">
            {data
              .find((s: any) => s.id === selected)
              .items.map((item: any, i: number) => (
                <Skill key={i} item={item} iconSize={44} />
              ))}
          </div>
        </div>
      ) : (
        ''
      )}
    </>
  );
};
