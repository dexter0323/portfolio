export const data = {
  menu: [
    {
      name: 'About',
      ref: '#about',
    },
    {
      name: 'Skills',
      ref: '#skills',
    },
    {
      name: 'Experience',
      ref: '#experience',
    },
    {
      name: 'Education',
      ref: '#education',
    },
  ],
  skills: [
    {
      id: 1,
      name: 'Frontend Development',
      description:
        'Structure and implementation of web designs with visual and interactive elements that users engage with through their devices when using a web application.',
      icon: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-8 h-8"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M9 17.25v1.007a3 3 0 01-.879 2.122L7.5 21h9l-.621-.621A3 3 0 0115 18.257V17.25m6-12V15a2.25 2.25 0 01-2.25 2.25H5.25A2.25 2.25 0 013 15V5.25m18 0A2.25 2.25 0 0018.75 3H5.25A2.25 2.25 0 003 5.25m18 0V12a2.25 2.25 0 01-2.25 2.25H5.25A2.25 2.25 0 013 12V5.25"
          />
        </svg>
      ),
      items: [
        {
          name: 'HTML5',
          img: 'html5',
        },
        {
          name: 'CSS3',
          img: 'css3',
        },
        {
          name: 'SCSS',
          img: 'sass',
        },

        {
          name: 'JavaScript',
          img: 'js',
        },
        {
          name: 'TypeScript',
          img: 'ts',
        },
        {
          name: 'Tailwind',
          img: 'tailwind',
        },
        {
          name: 'React',
          img: 'react',
        },
        {
          name: 'Svelte',
          img: 'svelte',
        },

        {
          name: 'Framework7',
          img: 'f7',
        },
        {
          name: 'Bootstrap',
          img: 'bootstrap',
        },
        {
          name: 'Materialize',
          img: 'materialize',
        },
        {
          name: 'Handlebars',
          img: 'handlebars',
        },
        {
          name: 'JQuery',
          img: 'jquery',
        },
        {
          name: 'Jest',
          img: 'jest',
        },
        {
          name: 'Mocha',
          img: 'mocha',
        },
        {
          name: 'Chai.js',
          img: 'chai.js',
        },
        {
          name: 'Sinon.js',
          img: 'sinon.js',
        },
        {
          name: 'Webpack',
          img: 'webpack',
        },
        {
          name: 'Rollup',
          img: 'rollup',
        },
        {
          name: 'Git',
          img: 'git',
        },
        {
          name: 'GitHub',
          img: 'github',
        },
        {
          name: 'GitLab',
          img: 'gitlab',
        },
      ],
    },
    {
      id: 2,
      name: 'Backend Development',
      description:
        'Creating, maintaining, testing, and debugging server side application or system, including API, core logic, databases, and application integrations.',
      icon: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          fill="currentColor"
          className="w-8 h-8"
        >
          <path
            fillRule="evenodd"
            d="M14.447 3.027a.75.75 0 01.527.92l-4.5 16.5a.75.75 0 01-1.448-.394l4.5-16.5a.75.75 0 01.921-.526zM16.72 6.22a.75.75 0 011.06 0l5.25 5.25a.75.75 0 010 1.06l-5.25 5.25a.75.75 0 11-1.06-1.06L21.44 12l-4.72-4.72a.75.75 0 010-1.06zm-9.44 0a.75.75 0 010 1.06L2.56 12l4.72 4.72a.75.75 0 11-1.06 1.06L.97 12.53a.75.75 0 010-1.06l5.25-5.25a.75.75 0 011.06 0z"
            clipRule="evenodd"
          />
        </svg>
      ),
      items: [
        {
          name: 'Node.js',
          img: 'node.js',
        },
        {
          name: 'Golang',
          img: 'go',
        },
        {
          name: 'NPM',
          bg: 'white',
          img: 'npm',
        },
        {
          name: 'JavaScript',
          img: 'js',
        },
        {
          name: 'TypeScript',
          img: 'ts',
        },
        {
          name: 'FP-TS',
          img: 'fp-ts',
        },
        {
          name: 'Kafka.js',
          img: 'kafka.js',
        },
        {
          name: 'MySQL',
          img: 'mysql',
        },
        {
          name: 'PostgreSQL',
          bg: 'white',
          img: 'postgresql',
        },
        {
          name: 'SQL Server',
          img: 'sqlserver',
        },
        {
          name: 'Oracle DB',

          img: 'oracledb',
        },
        {
          name: 'Mongo DB',
          img: 'mongodb',
        },
        {
          name: 'Document DB',

          img: 'documentdb',
        },
        {
          name: 'TypeORM',
          img: 'typeorm',
        },
        {
          name: 'Mongoose',
          img: 'mongoose',
        },
        {
          name: 'Sequelize',
          img: 'sequelize',
        },
        {
          name: 'Express.js',

          img: 'express.js',
        },
        {
          name: 'Koa.js',

          img: 'koa.js',
        },
        {
          name: 'OpenAPI',

          img: 'openapi',
        },
        {
          name: 'Swagger',
          img: 'swagger',
        },
        {
          name: 'Socket.IO',
          img: 'socket.io',
        },
        {
          name: 'Next.js',
          img: 'next.js',
        },
        {
          name: 'Electron.js',
          img: 'electron.js',
        },
        {
          name: 'Puppeteer',
          img: 'puppeteer',
        },
        {
          name: 'New Relic',
          img: 'newrelic',
        },
        {
          name: 'Jest',
          img: 'jest',
        },
        {
          name: 'Cucumber',
          img: 'cucumber',
        },
        {
          name: 'Mocha',
          img: 'mocha',
        },
        {
          name: 'Chai.js',
          img: 'chai.js',
        },
        {
          name: 'Sinon.js',
          img: 'sinon.js',
        },
        {
          name: 'Docker',
          img: 'docker',
        },
        {
          name: 'Git',
          img: 'git',
        },
        {
          name: 'GitHub',
          img: 'github',
        },
        {
          name: 'GitLab',
          img: 'gitlab',
        },
      ],
    },
    {
      id: 3,
      name: 'DevOps & Tools',
      description:
        'Familiarized with CI/CD best practices and tools, code repositories, cloud services, automated test, monitoring, and software development life cycle.',
      icon: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-8 h-8"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M6.75 7.5l3 2.25-3 2.25m4.5 0h3m-9 8.25h13.5A2.25 2.25 0 0021 18V6a2.25 2.25 0 00-2.25-2.25H5.25A2.25 2.25 0 003 6v12a2.25 2.25 0 002.25 2.25z"
          />
        </svg>
      ),
      items: [
        {
          name: 'AWS',
          img: 'aws',
        },
        {
          name: 'Docker',
          img: 'docker',
        },
        {
          name: 'Kubernetes',
          img: 'k8s',
        },
        {
          name: 'GitHub Actions',
          img: 'github',
        },
        {
          name: 'GitLab Pipelines',
          img: 'gitlab',
        },
        {
          name: 'Argo CD',
          img: 'argocd',
        },
        {
          name: 'JFrog Artifactory',
          img: 'jfrog',
        },
        {
          name: 'Datadog',
          bg: 'white',
          img: 'datadog',
        },
        {
          name: 'New Relic',
          img: 'newrelic',
        },
        {
          name: 'Okta',
          img: 'okta',
        },
        {
          name: 'Jira Software',
          img: 'jira',
        },
        {
          name: 'Confluence',
          img: 'confluence',
        },
        {
          name: 'Coda.io',
          img: 'coda',
        },
        {
          name: 'VS Code',
          img: 'vscode',
        },
        {
          name: 'IntelliJ IDEA',
          img: 'intellij',
        },
        {
          name: 'Postman',
          img: 'postman',
        },
        {
          name: 'DBeaver',
          img: 'dbeaver',
        },
        {
          name: 'MySQL Workbench',
          img: 'mysqlworkbench',
        },
        {
          name: 'pgAdming',
          bg: 'white',
          img: 'postgresql',
        },
        {
          name: 'Studio 3T',
          img: 'studio3t',
        },
        {
          name: 'Mongo DB Compass',
          img: 'compass',
        },

        {
          name: 'Adobe XD',
          img: 'xd',
        },
        {
          name: 'Figma',
          img: 'figma',
        },
        {
          name: 'Miro',
          img: 'miro',
        },
        {
          name: 'Draw.io',
          img: 'draw.io',
        },
        {
          name: 'Office 365',
          img: 'office365',
        },
        {
          name: 'G Suite',
          img: 'gsuite',
        },
        {
          name: 'Windows',
          img: 'windows',
        },
        {
          name: 'Mac OS',
          img: 'macos',
        },
        {
          name: 'Ubuntu',
          img: 'ubuntu',
        },
      ],
    },
  ],
  experience: [
    {
      id: 1,
      title: 'Full-Stack Engineer',
      location: 'BairesDev, SF, California, USA',
      description:
        'Deliver tailored services to clients, adapted to their needs, and navigate frontend and backend realms within projects. This demands versatile integration into diverse client ecosystems and technologies, all while guaranteeing the prosperous development and seamless delivery of web solutions.',
      buttonText: 'View Backend Projects',
      date: 'NOV 2022 - CURRENT',
      icon: 'work',
    },
    {
      id: 2,
      title: 'Project Leader',
      location: 'Banco Popular, Dominican Republic',
      description:
        'Lead, coordinate, and execute web development projects and technological solutions collaboratively. Employing Agile methodology, I ensure goal attainment and maintain proactive communication with internal clients to surpass their expectations. Additionally, I actively contribute to customer meetings, gathering and assessing application requirements and features.',
      buttonText: 'View Backend Projects',
      date: 'JUL 2021 - NOV 2022',
      icon: 'work',
    },
    {
      id: 3,
      title: 'Analyst Web Engineer',
      location: 'Banco Popular, Dominican Republic',
      description:
        'Delivered services and solutions to internal clients by developing and maintaining systems, applications, forms, and tools. These innovations streamlined process development, expediting desired outcomes. Actively engaged in meetings to comprehensively gather and evaluate customer requirements and features for applications.',
      buttonText: 'View Backend Projects',
      date: 'MAR 2018 - JUL 2021',
      icon: 'work',
    },
    {
      id: 4,
      title: 'Software Developer',
      location: 'Casa de Campo, Dominican Republic',
      description:
        'Designed and managed systems for seamless financial operations, and problem-solving. Collaborated with customers, utilizing Balsamiq Wireframes to craft interfaces, and modeling databases using Oracle SQL Developer. Developed applications, forms, reports, and web applications, spanning checkout, accounting, electricity, warehouse, and HR.',
      buttonText: 'View Backend Projects',
      date: 'JUL 2015 - MAR 2018',
      icon: 'work',
    },
    {
      id: 5,
      title: 'System and Computing Engineering',
      location: 'Dominican Republic',
      description: 'Universidad Dominicana O & M',
      date: 'SEP 2011 - DEC 2015',
      icon: 'school',
    },
  ],
};
