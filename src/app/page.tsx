import { About } from '@/app/components/About';
import { Header } from '@/app/components/Header';
import { Profile } from '@/app/components/Profile';
import { Skills } from '@/app/components/Skills';
import { Experience } from '@/app/components/Experience';
import { data } from '@/app/data';
import { Fragment } from 'react';

export default function Home() {
  return (
    <Fragment>
      <Header data={data.menu} />
      <Profile />
      <About />
      <Skills data={data.skills} />
      <Experience data={data.experience} />
    </Fragment>
  );
}
