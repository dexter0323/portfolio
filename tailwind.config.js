/** @type {import('tailwindcss').Config} */

const colors = {
  blue_aqua_light: '#CCFBFF',
  blue_aqua: '#99F8FF',
  blue_aqua_dark: '#00D6E6',
  black_main: '#232626',
  black_second: '#1A1C1D',
};

module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  // darkMode: true,
  theme: {
    extend: {
      backgroundColor: colors,
      color: colors,
      textColor: colors,
    },
  },
  plugins: [],
};
